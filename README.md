# Touch Sight

The goal of this project is to provide a physical interface for people who cannot see that gives them a sense of their surroundings. By using common WebXR techniques, the user's surrounding walls, floors and other surfaces (tables, doors, windows, etc) are translated into a "tactile" depth matte that they can "feel" by placing or dragging their fingers on the device and the device responding with haptic feedback at varying rythmys based on distance from the user. Haptic options are built-in: vibration or via [electroadhesion] (http://tanvas.co)
