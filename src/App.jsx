import React, { useRef, useEffect, useState } from 'react';
import { Haptics } from '@capacitor/haptics';
import testImage1 from './assets/square.jpg';
import testImage2 from './assets/star.jpg';
import testImage3 from './assets/stripes.jpg';
import testImage4 from './assets/person.jpg';
import testImage5 from './assets/square2.jpg';
import testImage6 from './assets/gradient.jpg';
import testImage7 from './assets/1.jpg';
import testImage8 from './assets/2.jpg';
import testImage9 from './assets/3.jpg';
import testImage10 from './assets/4.jpg';
import testImage11 from './assets/5.jpg';

let frequency = -1;
let context;
let touching2 = false;
const local = location.hostname === 'localhost';
const platform = navigator.userAgent.toLowerCase().indexOf('iphone') >= 0 ? 'device' : 'desktop';
alert(`local: ${local}, platform: ${platform}`);
function App() {
  const sampleSize = 4;
  const maxBrightness = 255;

  const images = [
    testImage1,
    testImage2,
    testImage3,
    testImage4,
    testImage5,
    testImage6,
    testImage7,
    testImage8,
    testImage9,
    testImage10,
    testImage11,
  ];

  const waitForSeconds = (secs) => new Promise((resolve) => { setTimeout(resolve, secs); });

  const canvas = useRef();
  const [touching, setTouching] = useState(false);
  const [vibrating, setVibrating] = useState(false);
  const [freq, setFreq] = useState(0);

  const vibrate = () => {
    setVibrating(true);

    if (platform !== 'device') {
      waitForSeconds(frequency).then(() => {
        setVibrating(false);
        if (touching2) {
          waitForSeconds(frequency).then(() => {
            if (touching2) {
              vibrate();
            }
          });
        }
      });
    } else {
      Haptics.vibrate({ duration: 200 }).then(() => {
        setVibrating(false);
        if (touching2) {
          waitForSeconds(frequency).then(() => {
            if (touching2) {
              vibrate();
            }
          });
        }
      });
    }
  };

  const brightnessBeneathCursor = (e) => {
    let x;
    let y;
    let total = 0;
    const samples = sampleSize * sampleSize;
    const {
      top, left, width, height,
    } = canvas.current.getBoundingClientRect(); // TODO: move this out and only update on resize
    const xScale = 500 / width;
    const yScale = 800 / height;

    if (e.changedTouches) {
      x = Math.round((e.changedTouches[0].clientX - left) * xScale);
      y = Math.round((e.changedTouches[0].clientY - top) * yScale);
    } else {
      x = Math.round(e.nativeEvent.offsetX * xScale);
      y = Math.round(e.nativeEvent.offsetY * yScale);
    }
    if (touching) {
      const { data } = context.getImageData(x - (sampleSize / 2), y - (sampleSize / 2), sampleSize, sampleSize);
      for (let i = 0; i < data.length; i += 4) {
        total += (data[i + 0] + data[i + 1] + data[i + 2]) / 3;
      }
    }
    return total / samples;
  };

  const touchStart = (e) => {
    touching2 = true;
    setTouching(true);
    const brightness = brightnessBeneathCursor(e);
    frequency = ((1.1 * maxBrightness) - brightness) * 2;
    setFreq(frequency);
    vibrate();
  };

  const touchEnd = () => {
    // TODO: make sure all touches are ended first
    touching2 = false;
    setTouching(false);
  };

  const touchMove = (e) => {
    if (touching) {
      const brightness = brightnessBeneathCursor(e);
      frequency = ((1.1 * maxBrightness) - brightness) * 2;
      setFreq(frequency);
    }
  };

  const drawImage = (file) => {
    const img = new Image();
    img.addEventListener('load', () => {
      const hRatio = canvas.current.width / img.width;
      const vRatio = canvas.current.height / img.height;
      const ratio = Math.min(hRatio, vRatio);
      context.drawImage(img, 0, 0, img.width, img.height, 0, 0, img.width * ratio, img.height * ratio);
    });
    img.src = file;
  };

  const randomize = () => {
    drawImage(images[Math.floor(Math.random() * images.length)]);
  };

  useEffect(() => {
    if (canvas.current) context = canvas.current.getContext('2d');
    randomize();
  }, []);

  return (
    <div className="app">
      <canvas
        ref={canvas}
        className="canvas"
        onMouseDown={touchStart}
        onMouseUp={touchEnd}
        onMouseMove={touchMove}
        onTouchStart={touchStart}
        onTouchEnd={touchEnd}
        onTouchMove={touchMove}
        width={500}
        height={800}
      />
      <p
        className="debug"
      >
        {freq.toFixed(1)}

      </p>
      <span className={`vibrating ${vibrating ? '' : 'hidden'}`} />
      <button
        type="button"
        className="randomize"
        onClick={randomize}
      >
        Randomize
      </button>
    </div>
  );
}

export default App;

// const processVideoFrame = async () => {
//   // Capture video frame
//   const context = canvasElement.getContext('2d');
//   context.drawImage(videoElement, 0, 0, canvasElement.width, canvasElement.height);

//   // Access the image data
//   const imageData = context.getImageData(0, 0, canvasElement.width, canvasElement.height);
//   const pixels = imageData.data;

//   // Preprocess the image data
//   const input = preprocessImage(imageData);

//   // Perform depth estimation using MIDAS
//   const depthMap = await estimateDepth(input);

//   // Postprocess the depth map if needed

//   // Render the processed depth map
//   renderDepthMap(depthMap);

//   // Schedule the next frame processing
//   requestAnimationFrame(processVideoFrame);
// };

// // Define the function to preprocess the image data
// const preprocessImage = (imageData) => {
//   const { width, height, data } = imageData;
//   const imageTensor = tf.browser.fromPixels(data).toFloat();
//   const resizedTensor = tf.image.resizeBilinear(imageTensor, [384, 384]);
//   const normalizedTensor = resizedTensor.div(255.0).expandDims();
//   return normalizedTensor;
// };

// // Define the function to estimate depth using MIDAS
// const estimateDepth = async (input) => {
//   const midasModel = await midas.load();
//   const depthMap = await midasModel.predict(input);
//   return depthMap;
// };

// // Define the function to render the depth map
// const renderDepthMap = (depthMap) => {
//   // Perform visualization or further processing
//   // Display the depth map on the canvas or any other desired output
// };

// // Start processing video frames
// videoElement.addEventListener('loadedmetadata', () => {
//   canvasElement.width = videoElement.videoWidth;
//   canvasElement.height = videoElement.videoHeight;
//   processVideoFrame();
// });
